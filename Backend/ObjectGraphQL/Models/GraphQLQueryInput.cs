﻿using System;
using System.Collections.Generic;

namespace ObjectGraphQL.Models
{
    public class GraphQLQueryInput
    {
        public string OperationName { get; set; }
        public string NamedQuery { get; set; }
        public string Query { get; set; }
        public Dictionary<string, object> Variables { get; set; }
    }
}
