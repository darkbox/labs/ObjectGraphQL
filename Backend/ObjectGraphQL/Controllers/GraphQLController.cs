using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GraphQL;
using GraphQL.Types;
using GraphQL.SystemTextJson;

namespace ObjectGraphQL.Controllers
{
    public class GraphQLController : Controller
    {
        public class GraphQLQueryInput
        {
            public string OperationName { get; set; }
            public string NamedQuery { get; set; }
            public string Query { get; set; }
            public Dictionary<string, object> Variables { get; set; }
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            return await Task.FromResult(View());
        }

        [HttpPost]
        public async Task<IActionResult> Index(
             [FromBody] GraphQLQueryInput Input
            )
        {
            var schema1 = Schema.For(@"
                type Droid {
                    id: ID!
                    name: String
                }

                type Query {
                    droid(id: ID!): Droid
                }
            ");

            
            var schema = new Schema()
            {
                Query = new BAG.RootQuery(),
                Mutation = new BAG.RootMutation(),
            };

            var result = await schema.ExecuteAsync(o =>
            {
                o.Query = Input.Query;
                o.OperationName = Input.OperationName;
                o.Inputs = Input.Variables?.ToInputs();
            });
            
            return Ok(result);
        }
    }
}

class BAG
{
    public static List<object> Space { get; }

    static BAG ()
    {
        Space = new List<object>();
    }

    class StaticType
    {
        internal class Output
        {
            internal class MyFieldPrototype
            {
                public string Key { get; set; }
                public string Name { get; set; }
                public string Type { get; set; }
            }
            internal class MyField : MyFieldPrototype
            {
                public string Value { get; set; }
            }
            internal class MySchema
            {
                public long Id { get; set; }
                public string Name { get; set; }
                public List<MyFieldPrototype> Fields { get; set; }
                public List<MyDynamic> Dynamics { get; set; }
            }
            internal class MyDynamic
            {
                public long Id { get; set; }
                public string Name { get; set; }
                public MySchema Schema { get; set; }
                public List<MyField> Fields { get; set; }
            }
        }

        internal class Input
        {
            internal class MyFieldPrototypeInput
            {
                public string Key { get; set; }
                public string Name { get; set; }
                public string Type { get; set; }
            }

            internal class MyFieldInput : MyFieldPrototypeInput
            {
                public string Value { get; set; }
            }

            internal class MySchemaInput
            {
                public string Name { get; set; }
                public List<MyFieldPrototypeInput> Fields { get; set; }
            }

            internal class MyDynamicInput
            {
                public string Name { get; set; }
                public long SchemaId { get; set; }
                public List<MyFieldInput> Fields { get; set; }
            }
        }
    }

    class StaticSchema
    {
        internal class Output
        {
            internal class MyFieldPrototype : ObjectGraphType
            {
                public MyFieldPrototype()
                {
                    Field<NonNullGraphType<StringGraphType>>("Name");
                    Field<NonNullGraphType<StringGraphType>>("Key");
                    Field<NonNullGraphType<StringGraphType>>("Type");
                }
            }

            internal class MyField : MyFieldPrototype
            {
                public MyField()
                    : base()
                {
                    Field<StringGraphType>("Value");
                }
            }

            internal class MySchema : ObjectGraphType
            {
                public MySchema()
                {
                    Field<NonNullGraphType<LongGraphType>>("Id");
                    Field<NonNullGraphType<StringGraphType>>("Name");
                    Field<NonNullGraphType<ListGraphType<MyFieldPrototype>>>("Fields");
                    Field<NonNullGraphType<ListGraphType<MyDynamic>>>("Dynamics");
                }
            }

            internal class MyDynamic : ObjectGraphType
            {
                public MyDynamic()
                {
                    Field<NonNullGraphType<LongGraphType>>("Id");
                    Field<NonNullGraphType<StringGraphType>>("Name");
                    Field<NonNullGraphType<MySchema>>("Schema");
                    Field<NonNullGraphType<ListGraphType<MyField>>>("Fields");
                }
            }

        }

        internal class Input
        {
            internal class MyFieldPrototypeInput : InputObjectGraphType
            {
                public MyFieldPrototypeInput()
                {
                    Field<NonNullGraphType<StringGraphType>>("Name");
                    Field<NonNullGraphType<StringGraphType>>("Key");
                    Field<NonNullGraphType<StringGraphType>>("Type");
                }
            }

            internal class MyFieldInput : MyFieldPrototypeInput
            {
                public MyFieldInput()
                    : base()
                {
                    Field<StringGraphType>("Value");
                }
            }

            internal class MySchemaInput : InputObjectGraphType
            {
                public MySchemaInput()
                {
                    Field<NonNullGraphType<StringGraphType>>("Name");
                    Field<NonNullGraphType<ListGraphType<MyFieldPrototypeInput>>>("Fields");
                }
            }
            internal class MyDynamicInput : InputObjectGraphType
            {
                public MyDynamicInput ()
                {
                    Field<NonNullGraphType<StringGraphType>>("Name");
                    Field<NonNullGraphType<LongGraphType>>("SchemaId");
                    Field<NonNullGraphType<ListGraphType<MyFieldInput>>>("Fields");
                }
            }
        }

    }

    internal class RootQuery : ObjectGraphType
    {
        public RootQuery()
        {
            // List Schemas
            Field<NonNullGraphType<ListGraphType<StaticSchema.Output.MySchema>>>()
                .Name("Schemas")
                .Argument<LongGraphType>("Id")
                .Argument<StringGraphType>("Name")
                .Resolve((ctx) =>
                {
                    var q = Space.Where(x => x is StaticType.Output.MySchema).Select(x => x as StaticType.Output.MySchema).AsQueryable();
                    if (ctx.HasArgument("Id") && ctx.GetArgument<long>("Id") is long id)
                    {
                        q = q.Where(s => s.Id == id);
                    }
                    if (ctx.HasArgument("Id") && ctx.GetArgument<string>("Name") is string name && !string.IsNullOrWhiteSpace(name))
                    {
                        q = q.Where(s => s.Name.Contains(name));
                    }
                    return q.ToList();
                });

            Field<NonNullGraphType<ListGraphType<StaticSchema.Output.MyDynamic>>>()
                .Name("Dynamics")
                .Argument<LongGraphType>("Id")
                .Argument<StringGraphType>("Name")
                .Resolve((ctx) =>
                {
                    var q = Space.Where(x => x is StaticType.Output.MyDynamic).Select(x => x as StaticType.Output.MyDynamic).AsQueryable();
                    if (ctx.HasArgument("Id") && ctx.GetArgument<long>("Id") is long id)
                    {
                        q = q.Where(s => s.Id == id);
                    }
                    if (ctx.HasArgument("Id") && ctx.GetArgument<string>("Name") is string name && !string.IsNullOrWhiteSpace(name))
                    {
                        q = q.Where(s => s.Name.Contains(name));
                    }
                    return q.ToList();
                });
        }
    }

    internal class RootMutation : ObjectGraphType
    {
        public RootMutation ()
        {
            Field<StaticSchema.Output.MySchema>()
                .Name("AddSchema")
                .Argument<NonNullGraphType<StaticSchema.Input.MySchemaInput>>("Schema")
                .Resolve((ctx) =>
                {
                    var inp = ctx.GetArgument<StaticType.Input.MySchemaInput>("Schema");

                    StaticType.Output.MySchema schema = new StaticType.Output.MySchema();

                    schema.Id = DateTime.UtcNow.Ticks;
                    schema.Name = inp.Name;
                    schema.Fields = inp.Fields
                    .Select(
                        f => new StaticType.Output.MyFieldPrototype()
                        {
                            Key = f.Key,
                            Name = f.Name,
                            Type = f.Type
                        }
                        )
                    .ToList();
                    schema.Dynamics = new List<StaticType.Output.MyDynamic>();

                    Space.Add(schema);

                    return schema;
                });
            Field<StaticSchema.Output.MyDynamic>()
                .Name("AddDynamic")
                .Argument<NonNullGraphType<StaticSchema.Input.MyDynamicInput>>("Dynamic")
                .Resolve((ctx) =>
                {
                    var inp = ctx.GetArgument<StaticType.Input.MyDynamicInput>("Dynamic");

                    var schema = Space.Where(x => x is StaticType.Output.MySchema)
                        .Select(x => x as StaticType.Output.MySchema)
                        .Where(s => s.Id == inp.SchemaId)
                        .FirstOrDefault();
                    if (schema != null)
                    {
                        StaticType.Output.MyDynamic dyn = new StaticType.Output.MyDynamic();

                        dyn.Id = DateTime.UtcNow.Ticks;
                        dyn.Name = inp.Name;
                        dyn.Fields = inp.Fields
                        .Select(
                            f => new StaticType.Output.MyField()
                            {
                                Key = f.Key,
                                Name = f.Name,
                                Type = f.Type
                            }
                            )
                        .ToList();
                        dyn.Schema = schema;

                        Space.Add(dyn);
                        schema.Dynamics.Add(dyn);
                        return dyn;
                    }
                    return null;
                });
        }
    }
}