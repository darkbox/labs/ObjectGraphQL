﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using GraphQL;
using GraphQL.Builders;
using GraphQL.Resolvers;
using GraphQL.SystemTextJson;
using GraphQL.Types;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using ObjectGraphQL.Models;
using System.Reflection.Emit;

namespace ObjectGraphQL
{
    class Faker
    {
        private static ModuleBuilder mod;

        public static Dictionary<string, Type> Types { get; } = new Dictionary<string, Type>();

        static Faker()
        {
            var assemblyBuilder = AssemblyBuilder.DefineDynamicAssembly(
                new System.Reflection.AssemblyName()
                {
                    Name = "_____VIRTUAL_STORAGE_ASSEMBLY_____"
                },
                System.Reflection.Emit.AssemblyBuilderAccess.Run
            );
            mod = assemblyBuilder.DefineDynamicModule("_____VIRTUAL_STORAGE_MODULE_____");

            MakeType("Hero", new Dictionary<string, Type>()
            {
                {"Name", typeof(string) },
                {"Level", typeof(short) },
            });
        }

        public static Type MakeType (string TypeName, Dictionary<string, Type> props)
        {
            if (!Types.ContainsKey(TypeName))
            {

                var tb = mod.DefineType(TypeName, System.Reflection.TypeAttributes.Class | System.Reflection.TypeAttributes.Public);
                foreach (var prop in props)
                {
                    var fB = tb.DefineField(prop.Key, prop.Value, FieldAttributes.Private);
                    var pB = tb.DefineProperty(prop.Key, PropertyAttributes.HasDefault, prop.Value, null);

                    // The property set and property get methods require a special
                    // set of attributes.
                    MethodAttributes getSetAttr =
                        MethodAttributes.Public | MethodAttributes.SpecialName |
                            MethodAttributes.HideBySig;

                    // Define the "get" accessor method for CustomerName.
                    MethodBuilder custNameGetPropMthdBldr =
                        tb.DefineMethod("get_" + prop.Key,
                                                   getSetAttr,
                                                   prop.Value,
                                                   Type.EmptyTypes);

                    ILGenerator custNameGetIL = custNameGetPropMthdBldr.GetILGenerator();

                    custNameGetIL.Emit(OpCodes.Ldarg_0);
                    custNameGetIL.Emit(OpCodes.Ldfld, fB);
                    custNameGetIL.Emit(OpCodes.Ret);

                    // Define the "set" accessor method for CustomerName.
                    MethodBuilder custNameSetPropMthdBldr =
                        tb.DefineMethod("set_" + prop.Key,
                                                   getSetAttr,
                                                   null,
                                                   new Type[] { prop.Value });

                    ILGenerator custNameSetIL = custNameSetPropMthdBldr.GetILGenerator();

                    custNameSetIL.Emit(OpCodes.Ldarg_0);
                    custNameSetIL.Emit(OpCodes.Ldarg_1);
                    custNameSetIL.Emit(OpCodes.Stfld, fB);
                    custNameSetIL.Emit(OpCodes.Ret);

                    // Last, we must map the two methods created above to our PropertyBuilder to
                    // their corresponding behaviors, "get" and "set" respectively.
                    pB.SetGetMethod(custNameGetPropMthdBldr);
                    pB.SetSetMethod(custNameSetPropMthdBldr);
                }

                Types.Add(TypeName, tb.CreateType());
            }
            return Types[TypeName];
        }
    }

    internal class GraphQLContext
    {
        static List<dynamic> Storage { get; } = new List<dynamic>();

        TypesClass InTypes = new TypesClass();
        TypesClass OutTypes = new TypesClass();

        class TypesClass
        {
            public IGraphType TableType { get; set; }
            public IGraphType ColumnType { get; set; }
            public List<IGraphType> CacheDynamics { get; } = new List<IGraphType>();
        }


        private HttpContext HttpContext;
        private Schema Schema;

        public GraphQLContext(HttpContext httpContext)
        {
            HttpContext = httpContext;
            Schema = new Schema();

            Schema.Query ??= new Query();
            Schema.Mutation ??= new Mutation();

            var level = 0;
            if (httpContext.Request.Headers.ContainsKey("Token") && httpContext.Request.Headers["Token"] is StringValues tokens && tokens.Count == 1 && tokens[0] is string token)
            {
                switch(token)
                {
                    case "ADMIN":
                        level = 2;
                        break;
                    case "USER":
                        level = 1;
                        break;
                    default:
                        level = 0;
                        break;
                }
            }

            BuildInTypes();
            BuildOutTypes();
            if (level >= 0) BuildSchema4Guest();
            if (level >= 1) BuildSchema4User();
            if (level >= 2) BuildSchema4Admin();
        }

        private void BuildInTypes()
        {
            // Static
            var ColumnType = new InputObjectGraphType()
            {
                Name = "_ColumnInput"
            };
            ColumnType.Field<NonNullGraphType<StringGraphType>>().Name("ColumnName");
            ColumnType.Field<NonNullGraphType<StringGraphType>>().Name("ColumnType");

            var TableType = new InputObjectGraphType()
            {
                Name = "_TableInput"
            };
            TableType.Field<NonNullGraphType<StringGraphType>>().Name("TableName");
            TableType.AddField(new FieldType()
            {
                Name = "Columns",
                ResolvedType = new NonNullGraphType(new ListGraphType(ColumnType))
            });

            InTypes.TableType = TableType;
            InTypes.ColumnType = ColumnType;

            // Dynamic
            foreach (var table in Storage.Where(x => x._SuperType == "STATIC"))
            {
                try
                {
                    var dynamicType = new InputObjectGraphType()
                    {
                        Name = table.TableName + "Input",
                    };
                    Dictionary<string, Type> props = new Dictionary<string, Type>()
                    {
                        {"_SuperType", typeof(string) },
                        {"_TableName", typeof(string) },
                    };
                    foreach (var column in table.Columns)
                    {
                        FieldBuilder<object, object> field;
                        Type fType;
                        switch (column.ColumnType)
                        {
                            case "STRING": field = dynamicType.Field<StringGraphType>(); fType = typeof(string); break;
                            case "SHORT": field = dynamicType.Field<ShortGraphType>(); fType = typeof(short); break;
                            case "INT": field = dynamicType.Field<IntGraphType>(); fType = typeof(int); break;
                            case "LONG": field = dynamicType.Field<LongGraphType>(); fType = typeof(long); break;
                            case "DATETIME": field = dynamicType.Field<DateTimeGraphType>(); fType = typeof(DateTime); break;
                            default: continue;
                        }
                        field.Name(column.ColumnName);
                        props.Add(column.ColumnName, fType);
                    }
                    Faker.MakeType(table.TableName, props);
                    InTypes.CacheDynamics.Add(dynamicType);
                }
                catch
                {

                }
            }
        }

        private void BuildOutTypes()
        {
            // Static
            var ColumnType = new ObjectGraphType()
            {
                Name = "_Column"
            };
            ColumnType.Field<NonNullGraphType<StringGraphType>>().Name("ColumnName");
            ColumnType.Field<NonNullGraphType<StringGraphType>>().Name("ColumnType");

            var TableType = new ObjectGraphType()
            {
                Name = "_Table"
            };
            TableType.Field<NonNullGraphType<StringGraphType>>().Name("TableName");
            TableType.AddField(new FieldType()
            {
                Name = "Columns",
                ResolvedType = new NonNullGraphType(new ListGraphType(ColumnType))
            });

            OutTypes.TableType = TableType;
            OutTypes.ColumnType = ColumnType;

            // Dynamic
            foreach (var table in Storage.Where(x => x._SuperType == "STATIC"))
            {
                try
                {
                    var dynamicType = new ObjectGraphType()
                    {
                        Name = table.TableName,
                    };
                    foreach(var column in table.Columns)
                    {
                        FieldBuilder<object, object> field;
                        switch (column.ColumnType)
                        {
                            case "STRING": field = dynamicType.Field<StringGraphType>(); break;
                            case "SHORT": field = dynamicType.Field<ShortGraphType>(); break;
                            case "INT": field = dynamicType.Field<IntGraphType>(); break;
                            case "LONG": field = dynamicType.Field<LongGraphType>(); break;
                            case "DATETIME": field = dynamicType.Field<DateTimeGraphType>(); break;
                            default: continue;
                        }
                        field.Name(column.ColumnName);
                    }

                    OutTypes.CacheDynamics.Add(dynamicType);
                }
                catch
                {

                }
            }
        }

        private void BuildSchema4Guest()
        {
            BuildStaticQuery();
            BuildDynamicQuery();
        }

        private void BuildSchema4User()
        {
            BuildDynamicMutation();
        }

        private void BuildSchema4Admin()
        {
            BuildStaticMutation();
        }

        private void BuildStaticQuery(){
            Schema.Query.AddField(new FieldType()
            {
                Name = "_Tables",
                ResolvedType = new ListGraphType(OutTypes.TableType),
                Resolver = new FuncFieldResolver<object>((ctx) =>
                {
                    return Storage
                        .Where(x => x._SuperType == "STATIC")
                        .Select(x => x as dynamic)
                        .ToList();
                })
            });
        }
        private void BuildDynamicQuery(){
            foreach(var dynamicType in OutTypes.CacheDynamics)
            {
                Schema.Query.AddField(new FieldType()
                {
                    Name = dynamicType.Name + "s",
                    ResolvedType = new ListGraphType(dynamicType),
                    Resolver = new FuncFieldResolver<object>((ctx) =>
                    {
                        return Storage
                            .Where(
                                x =>
                                    x._SuperType == "DYNAMIC"
                                    &&
                                    x._TableName == dynamicType.Name
                            );
                    })
                });
            }
        }
        private void BuildStaticMutation(){
            Schema.Mutation.AddField(new FieldType()
            {
                Name = "_AddTable",
                Arguments = new QueryArguments(
                    new QueryArgument(InTypes.TableType)
                    {
                        Name = "Table"
                    }
                ),
                ResolvedType = OutTypes.TableType,
                Resolver = new FuncFieldResolver<object>((ctx) =>
                {
                    if (ctx.HasArgument("Table"))
                    {
                        try
                        {
                            var inp = ctx.GetArgument<dynamic>("Table");
                            if (inp != null && !Storage.Any(x => x._SuperType == "STATIC" && x.TableName == inp["tableName"]))
                            {
                                var columns = new List<object>();
                                foreach(var col in inp["columns"])
                                {
                                    columns.Add(new
                                    {
                                        ColumnName = col["columnName"],
                                        ColumnType = col["columnType"],
                                    });
                                }
                                var table = new {
                                    _SuperType = "STATIC",
                                    TableName = inp["tableName"],
                                    Columns = columns,
                                };

                                Storage.Add(table);

                                return table;
                            }
                        }
                        catch
                        {

                        }
                    }
                    return null;
                })
            });
        }
        private void BuildDynamicMutation(){
            foreach (var dynamicType in OutTypes.CacheDynamics)
            {
                var dynamicInput = InTypes.CacheDynamics.FirstOrDefault(x => x.Name == dynamicType.Name + "Input");
                if (dynamicInput is null) continue;
                Schema.Mutation.AddField(new FieldType()
                {
                    Name = "Add" + dynamicType.Name + "",
                    Arguments = new QueryArguments(
                        new QueryArgument(dynamicInput)
                        {
                            Name = "Data"
                        }
                    ),
                    ResolvedType = dynamicType,
                    Resolver = new FuncFieldResolver<object>((ctx) =>
                    {
                        var tableName = dynamicType.Name;
                        if (ctx.HasArgument("Data"))
                        {
                            try
                            {
                                var table = Storage.First(x => x._SuperType == "STATIC" && x.TableName == tableName);
                                var dynType = Faker.Types[tableName];

                                var inp = ctx.GetArgument<dynamic>("Data");
                                if (inp != null)
                                {
                                    var obj = Activator.CreateInstance(dynType);
                                    dynType.GetProperty("_SuperType").SetValue(obj, "DYNAMIC");
                                    dynType.GetProperty("_TableName").SetValue(obj, tableName);
                                    foreach (var col in table.Columns)
                                    {
                                        string colName = col.ColumnName + "";
                                        dynType.GetProperty(colName).SetValue(obj, inp[colName.ToCamelCase()]);
                                    }

                                    Storage.Add(obj);

                                    return obj;
                                }
                            }
                            catch
                            {

                            }
                        }
                        return null;
                    })
                });
            }
        }

        internal async Task<string> Process(GraphQLQueryInput Input)
        {
            //throw new NotImplementedException();
            return await Schema.ExecuteAsync(o =>
            {
                o.Query = Input.Query;
                o.OperationName = Input.OperationName;
                o.Inputs = Input.Variables?.ToInputs();
            });
        }

        private class Query : ObjectGraphType
        {
            public Query ()
            {
                Field<StringGraphType>()
                    .Name("Name")
                    .Resolve((ctx) =>
                    {
                        return "Query";
                    });
            }
        }

        private class Mutation : ObjectGraphType
        {
            public Mutation()
            {
                Field<StringGraphType>()
                    .Name("Name")
                    .Resolve((ctx) =>
                    {
                        return "Mutation";
                    });
            }
        }
    }
}

public class DynamicDictionary : DynamicObject
{
    // The inner dictionary.
    Dictionary<string, object> dictionary
        = new Dictionary<string, object>();

    // This property returns the number of elements
    // in the inner dictionary.
    public int Count
    {
        get
        {
            return dictionary.Count;
        }
    }

    // If you try to get a value of a property
    // not defined in the class, this method is called.
    public override bool TryGetMember(
        GetMemberBinder binder, out object result)
    {
        // Converting the property name to lowercase
        // so that property names become case-insensitive.
        string name = binder.Name.ToLower();

        // If the property name is found in a dictionary,
        // set the result parameter to the property value and return true.
        // Otherwise, return false.
        return dictionary.TryGetValue(name, out result);
    }

    // If you try to set a value of a property that is
    // not defined in the class, this method is called.
    public override bool TrySetMember(
        SetMemberBinder binder, object value)
    {
        // Converting the property name to lowercase
        // so that property names become case-insensitive.
        dictionary[binder.Name.ToLower()] = value;

        // You can always add a value to a dictionary,
        // so this method always returns true.
        return true;
    }
}